import {setPannerPosition} from "./lib/utils.js";
import {Oscillator} from "./lib/osc.js";
import {Envelope} from "./lib/envelope.js";
import {NotePlayer} from "./lib/notePlayer.js";
import {Oscilloscope} from "./lib/oscilloscope.js";

// Globals
const maxDelayFeedback = 1;
const maxDelayTime = 1; //seconds
const filterTypes = ["lowpass", "highpass", "bandpass"];
export class Synth {

  // State properties
  #keyOctSwitch = 0;
  #keyDownArray = [];
  #oscillators = [];

  // Audio engine
  #context;
  #panner;
  #player; // todo: include player in the class?

  // Effects
  #tuna; // effects library
  // todo: create wrapper class Envelope
  #envelope;

  // todo: create wrapper class Filter
  #filter;
  #filterGain;

  // todo: create wrapper class Tremolo
  #tremolo;
  #tremoloGain;

  // todo: create wrapper class Overdrive
  #overdrive;
  #overdriveLevel;

  // todo: create wrapper class Delay
  #delay;
  #delayFeedback;
  #delayWetLevel;

  #oscilloscope;

  constructor(audioContext, NUM_OSC) {
    this.#context = audioContext;
    this.#panner = this.#context.createPanner();
    setPannerPosition(this.#panner, this.#context, 1, 0, 0);
    for (let i = 0; i < NUM_OSC; ++i) {
      this.#oscillators.push(new Oscillator(this.#context, this.#panner));
    }
    this.#envelope = new Envelope(); // for now it's just a container of params
    this.#player = new NotePlayer(this.#oscillators, this.#envelope);
    this.#oscilloscope = new Oscilloscope(this.#context, this.#context.destination);

    // FX
    this.#tuna = new Tuna(this.#context);

    this.#overdrive = new this.#tuna.Overdrive({outputGain: 0.5});
    this.#tremolo = new this.#tuna.Tremolo();
    this.#filter = new this.#tuna.Filter({gain: 0}); // for now we don't change the inner gain param

    this.#delay = this.#context.createDelay();
    this.#delay.delayTime.value = 0; //150 ms delay
    this.#delayFeedback = this.#context.createGain();
    this.#delayFeedback.gain.value = 0;
    this.#delayWetLevel = this.#context.createGain();
    this.#delayWetLevel.gain.value = 0;


    // SIGNAL ROUTING
    this.#panner.connect(this.#tremolo.input);
    this.#tremolo.connect(this.#overdrive.input);
    this.#overdrive.connect(this.#filter);
    this.#filter.connect(this.#oscilloscope.analyser);
    this.#oscilloscope.analyser.connect(this.#delay);
    this.#oscilloscope.analyser.connect(this.#context.destination); // delay dry signal
    this.#delay.connect(this.#delayFeedback);
    this.#delay.connect(this.#delayWetLevel);
    this.#delayFeedback.connect(this.#delay);
    this.#delayWetLevel.connect(this.#context.destination);
  }

  // ---------------------------
  // Key Events
  // ---------------------------
  getKeyDownArray() {
    return this.#keyDownArray; // todo: should not expose the array
  }

  isKeyDown(idx) {
    return this.#keyDownArray[idx];
  }

  setKeyDown(idx, flag) {
    this.#keyDownArray[idx] = flag;
  }

  // ---------------------------
  // Octave
  // ---------------------------
  getKeyOctaveSwitch() {
    return this.#keyOctSwitch;
  }

  setKeyOctaveSwitch(value) {
    this.#keyOctSwitch = value;
  }

  // ---------------------------
  // Oscillator
  // ---------------------------
  oscillatorGet(idx) {
    const osc = this.#oscillators[idx];
    return {
      volume: osc.volume,
      type: osc.vco.type,
      octave: osc.octave,
      detune: osc.detune,
    }
  }

  oscillatorSet(idx, settings) {
    const osc = this.#oscillators[idx];
    if (settings.volume !== undefined) {
      osc.volume = settings.volume;
    }
    if (settings.type !== undefined) {
      osc.vco.type = settings.type;
    }
    if (settings.octave !== undefined) {
      osc.octave = settings.octave;
    }
    if (settings.detune !== undefined) {
      osc.detune = settings.detune;
    }
  }

  // ---------------------------
  // Filter
  // ---------------------------
  filterGet() {
    return {
      frequency: this.#filter.frequency,
      Q: this.#filter.Q,
      // gain: this.#filter.gain,
      // let's use separate gain node
      gain: this.#filterGain.value,
      bypass: this.#filter.bypass,
      filterType: this.#filter.filterType,
    };
  }

  filterSet(settings) {
    if (settings.frequency) {
      this.#filter.frequency = settings.frequency;
    }
    if (settings.Q) {
      this.#filter.Q = settings.Q;
    }
    if (settings.gain) {
      this.#filter.output.value = settings.gain; // todo: fix filter gain
    }
    if (settings.bypass) {
      this.#filter.bypass = settings.bypass;
    }
    if (settings.filterType) {
      this.#filter.filterType = filterTypes[settings.filterType];
    }
  }

  filterOn() {
    this.#filter.bypass = false;
  }

  filterOff() {
    this.#filter.bypass = true;
  }

  // ---------------------------
  // Tremolo
  // ---------------------------
  tremoloGet() {
    return {
      bypass: this.#tremolo.bypass,
      intensity: this.#tremolo.intensity,
      rate: this.#tremolo.rate,
      stereoPhase: this.#tremolo.stereoPhase,
    }
  }

  tremoloSet(settings) {
    if (settings.bypass) {
      this.#tremolo.bypass = settings.bypass;
    }
    if (settings.intensity) {
      this.#tremolo.intensity = settings.intensity;
    }
    if (settings.rate) {
      this.#tremolo.rate = settings.rate;
    }
    if (settings.stereoPhase) {
      this.#tremolo.stereoPhase = settings.stereoPhase;
    }
  }

  tremoloOn() {
    this.#tremolo.bypass = false;
  }

  tremoloOff() {
    this.#tremolo.bypass = true;
  }

  // ---------------------------
  // Overdrive
  // ---------------------------
  overdriveGet() {
    return {
      bypass: this.#overdrive.bypass,
      drive: this.#overdrive.drive,
      curveAmount: this.#overdrive.curveAmount,
      algorithmIndex: this.#overdrive.algorithmIndex,
      // outputGain: this.#overdrive.outputGain,
      level: this.#overdriveLevel.value,
    }
  }

  overdriveSet(settings) {
    if (settings.bypass) {
      this.#overdrive.bypass =  settings.bypass;
    }
    if (settings.drive) {
      this.#overdrive.drive = settings.drive;
    }
    if (settings.curveAmount) {
      this.#overdrive.curveAmount = settings.curveAmount;
    }
    if (settings.algorithmIndex) {
      this.#overdrive.algorithmIndex = settings.algorithmIndex;
    }
    if (settings.level) {
      this.#overdrive.outputGain.value = settings.level;
    }
  }

  overDriveOn(){
    this.#overdrive.bypass = false;
  }

  overDriveOff(){
    this.#overdrive.bypass = true;
  }

  // ---------------------------
  // Delay
  // ---------------------------
  delayGet() {
    return {
      time: this.#delay.delayTime.value,
      feedback: this.#delayFeedback.gain.value,
      wetLevel: this.#delayWetLevel.gain.value,
    }
  }

  delaySet(settings) {
    if (settings.time) {
      this.#delay.delayTime.value = maxDelayTime * settings.time;
    }
    if (settings.feedback) {
      this.#delayFeedback.gain.value = maxDelayFeedback * settings.feedback;
    }
    if (settings.wetLevel) {
      this.#delayWetLevel.gain.value = settings.wetLevel;
    }

  }

  // ---------------------------
  // Envelope
  // ---------------------------
  envelopeGet() {
    return {
      attack: this.#envelope.attack,
      release: this.#envelope.release,
      portamento: this.#envelope.portamento,
    }
  }

  envelopeSet(settings) {
    if (settings.attack) {
      this.#envelope.attack = settings.attack;
    }
    if (settings.release) {
      this.#envelope.release = settings.release;
    }
    if (settings.portamento) {
      this.#envelope.portamento = settings.portamento;
    }
  }

  // ---------------------------
  // Player
  // ---------------------------
  play(note) {
    this.#player.play(note, this.#keyOctSwitch);
  }

  mute(note) {
    this.#player.mute(note, this.#keyOctSwitch);
  }
}
