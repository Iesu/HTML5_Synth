import {BROWSER} from "./browser.js";

export function noteToFrequency(note) {
  return Math.pow(2, (note - 58) / 12) * 440.0;
}

export function getAudioContext(browser) {
  try {
    // Fix up for prefixing
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    const context = new AudioContext();
    if (browser === BROWSER.CHROME) {
      context.resume();
    }
    return context;
  } catch (e) {
    alert('Web Audio API is not supported in this browser');
    throw e;
  }
}

export function setPannerPosition(panner, ctx, x, y, z) {
  panner.orientationX.setValueAtTime(x, ctx.currentTime);
  panner.orientationY.setValueAtTime(y, ctx.currentTime);
  panner.orientationZ.setValueAtTime(z, ctx.currentTime);
}


export function roundNum(num, scale) {
  scale = scale || 0;
  if(!("" + num).includes("e")) {
    return +(Math.round(num + "e+" + scale)  + "e-" + scale);
  } else {
    var arr = ("" + num).split("e");
    var sig = ""
    if(+arr[1] + scale > 0) {
      sig = "+";
    }
    return +(Math.round(+arr[0] + "e" + sig + (+arr[1] + scale)) + "e-" + scale);
  }
}
