"use strict";

import {detectOS} from "./lib/os.js";
import {detectBrowser} from "./lib/browser.js";
import {getAudioContext, roundNum} from "./lib/utils.js";
import {WAVEFORMS_LIST} from "./lib/osc.js";
import {Synth} from "./synth.js";

// **********************************************************************
// GLOBALS
// **********************************************************************
const MAX_OCTAVE = 2;
const TREM_RATE_MULTIPLIER = 12;
const OD_LEVEL_FACTOR = 100;
const ATT_OFFSET = 0.001;
const REL_OFFSET = 0.01;
const PORTAMENTO_FACTOR = 500; // reduces the max speed of portamento parameter


function drawKeyboard(keyDownArray) {

  const keyboardWidth = $("#keyboardContainer").width();
  const NUM_KEYS = 48;
  const numOctaves = (NUM_KEYS / 12);
  const numWhiteKeys = numOctaves * 7;
  const whiteKeyWidth = (keyboardWidth / numWhiteKeys);
  const blackKeyWidth = whiteKeyWidth * 0.6;
  $('#keyboard').css("height", 0.1 * keyboardWidth);

  // White Keys
  let j = 0
  let wk = 0
  let bk = 0
  let keyId = "k";
  let posX = 0;

  for (let i = 0; i < NUM_KEYS; i++) {
    j = i % 12;
    keyDownArray[i] = false;
    keyId = "k" + i;
    if (j == 1 || j == 3 || j == 6 || j == 8 || j == 10) {
      posX = (wk) * whiteKeyWidth - blackKeyWidth / 2;
      $('#keyboard').append('<div id="' + keyId + '" data-keyNum="' + (i + 1) + '" class="key blackKey" ></div>');
      $("#" + keyId).css("left", posX);
      bk++;
    } else {
      $('#keyboard').append('<div id="' + keyId + '" data-keyNum="' + (i + 1) + '" class="key whiteKey" ></div>');
      $("#" + keyId).css("left", wk * (whiteKeyWidth));
      wk++;
    }
  }
  $('.whiteKey').css("width", (whiteKeyWidth - 2) + "px");
  $('.blackKey').css("width", blackKeyWidth + "px");


  // Draw synth frame
  $('.frame_left').css('height', $('.center_section').height() + "px");
  $('.frame_right').css('height', $('.center_section').height() + "px");

}

// **********************************************************************
// MAIN
// **********************************************************************
$(document).ready(() => {

  const NUM_OSC = $('.oscillator').length;
  const os = detectOS();
  const browser = detectBrowser();
  console.log("OS: " + os);
  console.log("Browser: " + browser);
  const context = getAudioContext(browser);

  const synth = new Synth(context, NUM_OSC);
  // Set Synth according to UI defaults
  synth.filterSet({
    frequency: 1 + 20 * parseFloat($('#knob-filterFreq').attr('data-value')),       //20 to 2000
    Q: 1 + 49 * parseFloat($('#knob-filterQ').attr('data-value')) / 100,                  //0.001 to 100
    gain: parseFloat($('#knob-filterGain').attr('data-value')) / 100,
    bypass: true,             //0 to 1+
    filterType: parseInt($('#knob-filterType').attr('data-value')),         //0 to 7, corresponds to the filter types in the native filter node: lowpass, highpass, bandpass, lowshelf, highshelf, peaking, notch, allpass in that order
  });
  synth.tremoloSet({
    intensity: parseFloat($('#knob-tremoloIntensity').attr('data-value')) / 100,    //0 to 1
    rate: TREM_RATE_MULTIPLIER * parseFloat($('#knob-tremoloRate').attr('data-value')) / 100,         //0.001 to 8
    stereoPhase: 0,    //0 to 180
    bypass: true,
  });
  synth.overdriveSet({
    drive: 0.7,              //0 to 1
    curveAmount: parseFloat($('#knob-drive').attr('data-value')) / 100,          //0 to 1
    algorithmIndex: 0,       //0 to 5, selects one of our drive algorithms
    bypass: true,
    level: parseFloat($('#knob-odLevel').attr('data-value')) / OD_LEVEL_FACTOR,
  });
  synth.envelopeSet({
    portamento: parseFloat($('#knob-portamento').attr('data-value')) / PORTAMENTO_FACTOR,
    attack: ATT_OFFSET + parseFloat($('#knob-attack').attr('data-value')) / 100,
    release: REL_OFFSET + ($('#knob-release').attr('data-value')) / 100,
  });
  synth.delaySet({
    time: parseFloat($('#knob-delayTime').attr('data-value')) / 100,
    feedback: parseFloat($('#knob-delayFeedback').attr('data-value')) / 100,
    wetLevel: parseFloat($('#knob-delayVolume').attr('data-value')) / 100,
  });

  drawKeyboard(synth.getKeyDownArray());

  // ******************************************************
  // KEYBOARD EVENTS
  // ******************************************************

  //let keyValues = { 90:"c", 83:"c#", 88:"d", 68:"d#", 67:"e", 86:"f", 71:"f#" , 66:"g", 72:"g#", 78:"a", 74:"a#", 77:"b"};
  // const noteNames = new Array("c", "c#", "d", "d#", "e", "f", "f#", "g", "g#", "a", "a#", "b");
  const keyValues = {
    90: 1, 83: 2, 88: 3, 68: 4, 67: 5, 86: 6, 71: 7, 66: 8, 72: 9, 78: 10, 74: 11, 77: 12,
    81: 13, 50: 14, 87: 15, 51: 16, 69: 17, 82: 18, 53: 19, 84: 20, 54: 21, 89: 22, 55: 23, 85: 24,
    73: 25, 57: 26, 79: 27, 48: 28
  };

  keyValues[70] = -1;
  keyValues[75] = -1;
  keyValues[52] = -1;
  keyValues[56] = -1;

  let keyPressed = -1;
  let note = -1;
  $(document).keydown(function (event) {
    keyPressed = event.which;
    note = (keyValues[keyPressed]);
    if (keyPressed == 40) { // Arrow Key down
      /*
      // Move the octave knob
      octave1 = parseInt( $('#knob-octave1').attr("data-value") );
      if( octave1>0 ) $('#knob-octave1').attr('data-value' , --octave1);
      steps = parseInt( $('#knob-octave1').attr("data-steps") );
      $('#knob-octave1  .marker_container').css('transform', "rotate("+parseFloat(280.0*(octave1/steps))+"deg)");
      */
      synth.setKeyOctaveSwitch(synth.getKeyOctaveSwitch() - 1) % MAX_OCTAVE;
    }
    if (keyPressed == 38) { // Arrow Key Up
      /*
      // Move the octave knob
      octave1 = parseInt( $('#knob-octave1').attr("data-value") );
      if( octave1<5 ) $('#knob-octave1').attr('data-value' , ++octave1);
      steps = parseInt( $('#knob-octave1').attr("data-steps") );
      $('#knob-octave1  .marker_container').css('transform', "rotate("+parseFloat(280.0*(octave1/steps))+"deg)");
      */
      synth.setKeyOctaveSwitch = (synth.getKeyOctaveSwitch() + 1) % MAX_OCTAVE;
    } else {
      if (note > 0 && !synth.isKeyDown(keyValues[event.which])) {
        // check note validity and avoid bounce
        context.resume().then(() => synth.play(note));
      }
      //alert(keyPressed);
      $('#k' + (keyValues[event.which] - 1)).addClass('active');
      synth.setKeyDown(keyValues[event.which], true);
    }
  });

  $(document).keyup(function (event) {
    $('#k' + (keyValues[event.which] - 1)).removeClass('active');
    keyPressed = event.which;
    note = (keyValues[keyPressed]);
    synth.mute(note)
    synth.setKeyDown(keyValues[event.which], false);
  });

  // ******************************************************
  // MOUSE EVENTS
  // ******************************************************
  let isMouseDown = false;

  $(document).mouseup(function () {
    isMouseDown = false;
  });

  $(document).mousedown(function () {
    isMouseDown = true;
  });

  // MOUSE NOTE ON
  $('.key').mousedown(function () {
    note = parseInt($(this).attr("data-keyNum"));
    $(this).addClass("active");
    context.resume().then(() => synth.play(note));
  });

  $(".key").mouseover(function () {
    if (isMouseDown) {
      $(this).addClass("active");
      context.resume().then(() => synth.play(parseInt($(this).attr("data-keyNum"))));
    }
  });

  // MOUSE NOTE OFF
  $(".key").mouseup(function () {
    $(this).removeClass("active");
    synth.mute(parseInt($(this).attr("data-keyNum")));
  });
  $(".key").mouseleave(function () {
    $(this).removeClass("active");
    synth.mute(parseInt($(this).attr("data-keyNum")));
  });


  // ******************************************************
  // KNOB EVENTS
  // ******************************************************

  $('.knob').click(function () {
    parseKnobParam($(this).attr('id'));
  });
  $('.knob').mousemove(function () {
    if (isMouseDown) parseKnobParam($(this).attr('id'));
  });


  const parseKnobParam = function (id) {
    // todo: move to external method
    const valueStr = $('#' + id).attr('data-value');
    const value = roundNum(parseFloat(valueStr), 3);
    // Oscillator controls
    if (id.includes('knob-volume')) {
      const idx = parseInt(id.substr("knob-volume".length), 10);
      synth.oscillatorSet(idx - 1, {volume: value / 100});
    } else if (id.includes('knob-waveform-')) {
      const idx = parseInt(id.substr("knob-waveform-".length), 10);
      synth.oscillatorSet(idx - 1, {type: WAVEFORMS_LIST[roundNum(value)]});
    } else if (id.includes('knob-octave')) {
      const idx = parseInt(id.substr("knob-octave".length), 10);
      synth.oscillatorSet(idx - 1, {octave: 1 + roundNum(value)});
    } else if (id.includes('knob-detune')) {
      const idx = parseInt(id.substr("knob-detune".length), 10);
      synth.oscillatorSet(idx - 1, {detune: roundNum((value - 50) * .02, 3)});
    }

    // Delay controls
    else if (id == 'knob-delayTime') {
      synth.delaySet({time: roundNum(value * .01, 3)});
    } else if (id == 'knob-delayFeedback') {
      synth.delaySet({feedback: roundNum(value * .01, 3)});
    } else if (id == 'knob-delayVolume') {
      synth.delaySet({wetLevel: roundNum(value * .01, 3)});
    }

    // Envelope controls
    else if (id == 'knob-attack') {
      synth.envelopeSet({attack: roundNum(ATT_OFFSET + value * .01, 3)});
    } else if (id == 'knob-decay') {
      synth.envelopeSet({decay: roundNum(value * .01, 3)});
    } else if (id == 'knob-sustain') {
      synth.envelopeSet({sustain: roundNum(value * .01, 3)});
    } else if (id == 'knob-release') {
      synth.envelopeSet({release:  roundNum(REL_OFFSET + value * .01, 3)});
    } else if (id == 'knob-portamento') {
      synth.envelopeSet({portamento: roundNum(value / PORTAMENTO_FACTOR, 3)});
    }


    // Tremolo controls
    else if (id == 'knob-tremoloRate') {
      synth.tremoloSet({rate: roundNum(TREM_RATE_MULTIPLIER * value * .01, 3)});
    } else if (id == 'knob-tremoloIntensity') {
      synth.tremoloSet({intensity: roundNum(value * .01, 3)});
    }

    // Filter controls
    else if (id == 'knob-filterFreq') {
      synth.filterSet({frequency: 20 + 20 * roundNum(value, 3)});
    } else if (id == 'knob-filterQ') {
      synth.filterSet({Q: 1 + 49 * roundNum(value * .01, 3)});
    } else if (id == 'knob-filterType') {
      synth.filterSet({filterType: roundNum(value, 3)});
    } else if (id == 'knob-filterGain') {
      synth.filterSet({gain: roundNum(1 + .01 * (2 * (value - 50)), 3)});
    }

    // Overdrive controls
    else if (id == 'knob-drive') {
      synth.overdriveSet({curveAmount: roundNum(value * .01, 3)});
    } else if (id == 'knob-odLevel') {
      synth.overdriveSet({level: roundNum(value / OD_LEVEL_FACTOR, 3)});
    }

  };

  // Set initial oscillator values from UI
  for (let i = 1; i <= NUM_OSC; ++i) {
    parseKnobParam("knob-waveform-" + i);
    parseKnobParam("knob-volume" + i);
    parseKnobParam("knob-octave" + i);
    parseKnobParam("knob-detune" + i);
  }

  // ******************************************************
  // SWITCH EVENTS
  // ******************************************************
  let id = null;
  let val = null;
  $('.switchContainer').mouseup(function () {
    id = $(this).attr('id');
    val = parseFloat($(this).attr('data-value'));
    if (id == 'sw-tremolo') {
      $(this).attr('data-value', val);
      val == 0 ? synth.tremoloOff() : synth.tremoloOn();
    } else if (id == 'sw-filter') {
      val == 0 ? synth.filterOff() : synth.filterOn();
    } else if (id == 'sw-overdrive') {
      $(this).attr('data-value', val);
      val == 0 ? synth.overDriveOff() : synth.overDriveOn();
    }
  });

});
